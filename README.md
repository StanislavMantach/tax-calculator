# README #

Tax Calculator Rest Service.

I wrote this application with TDD approach to test all the calculations and used Spring Framework. in particular Spring Boot and Spring Rest.

Application Exposes Rest Service http://localhost:8080/api/v1/tax/{wageAmount}/

Web service returns calculation for: 
Five levels of Trinnskatt tax;
Lønnsinntekt case of Trygdeavgift tax;

All the verifications you can find in unit tests;

TrinnskattCalculatorTest
TrygdeavgiftCalculatorTest

To design unit tests I used 
SPECIFICATION-BASED (BLACK-BOX) TECHNIQUES
    * Equivalence partitioning
    * Boundary value analysis
