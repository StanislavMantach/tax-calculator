package org.steria.calculator.rest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.steria.calculator.service.TaxService;
import org.steria.calculator.service.TaxServiceV1;
import org.steria.calculator.service.domain.TaxCalculation;

/**
 * Created by smanta on 06.03.2018.
 */

@RunWith(MockitoJUnitRunner.class)
public class TaxControllerTest {

    @InjectMocks
    private TaxController controller = new TaxController();

    @Mock
    private TaxService taxService = new TaxServiceV1();

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void taxUnitTest() throws Exception {
        ResponseEntity<TaxCalculation> responseEntity = controller.tax(1000000d);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        responseEntity = controller.tax(-1d);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_ACCEPTABLE);
    }

}
