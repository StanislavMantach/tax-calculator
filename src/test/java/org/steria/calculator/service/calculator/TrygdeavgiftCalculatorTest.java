package org.steria.calculator.service.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.tax.trygdeavgift.Trygdeavgift;
import org.steria.calculator.service.domain.tax.trygdeavgift.TrygdeavgiftLevel;

import static org.steria.calculator.service.predicate.TrygdeavgiftPredicates.TRYGDEAVGIFT_L0_TOP;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class TrygdeavgiftCalculatorTest {

    private TaxCalculator<Trygdeavgift> calculator;

    @Before
    public void setUp(){
        calculator = new TrygdeavgiftCalculatorV1();
    }

    @Test
    public void getL0TrygdeavgiftBoundaryTaxTest() throws Exception {
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(50000d));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 0);

        trygdeavgift= calculator.calculate(new Income(TRYGDEAVGIFT_L0_TOP));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 0);

        trygdeavgift= calculator.calculate(new Income(TRYGDEAVGIFT_L0_TOP-1));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 0);
    }

    @Test
    public void getL1TrygdeavgiftBoundaryTaxTest() throws Exception {
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(TRYGDEAVGIFT_L0_TOP+1));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 0.25);

        trygdeavgift = calculator.calculate(new Income(TRYGDEAVGIFT_L0_TOP+1000));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 250);

        trygdeavgift = calculator.calculate(new Income(TRYGDEAVGIFT_L0_TOP+10000));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 2500);
    }

    @Test
    public void getReguiredTrygdeavgiftTaxTest() throws Exception {
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(60000d));
        Assert.assertEquals(trygdeavgift.getEstimatedTax(),1338d, 1);


        trygdeavgift = calculator.calculate(new Income(75000d));
        Assert.assertEquals(trygdeavgift.getEstimatedTax(), 5088d, 1);

        trygdeavgift = calculator.calculate(new Income(100000d));
        Assert.assertEquals(trygdeavgift.getEstimatedTax(),8200d, 1);
    }

    @Test
    public void getMillionAndHalfTrygdeavgiftTaxTest() throws Exception {
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(500000d));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 41000d);

        trygdeavgift = calculator.calculate(new Income(1000000d));
        Assert.assertTrue(trygdeavgift.getEstimatedTax() == 82000d);
    }

    @Test
    public void addBasisForTrygdeavgift(){
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(50000d));
        Assert.assertNotNull(trygdeavgift.getBasis());
    }

    @Test
    public void getFirstTrygdeavgiftType(){
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(50000d));
        Assert.assertEquals(trygdeavgift.getCategory(), TrygdeavgiftLevel.TRYG_0);
        Assert.assertTrue(TrygdeavgiftLevel.TRYG_0.getPst() == 0);
    }


    @Test
    public void getSecondTrygdeavgiftType(){
        Trygdeavgift trygdeavgift = calculator.calculate(new Income(60000d));
        Assert.assertEquals(trygdeavgift.getCategory(), TrygdeavgiftLevel.TRYG_1);
        Assert.assertTrue(TrygdeavgiftLevel.TRYG_1.getPst() == 0.082);
    }

}
