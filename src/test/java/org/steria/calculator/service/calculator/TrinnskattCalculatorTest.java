package org.steria.calculator.service.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.tax.trinnskat.Trinnskatt;
import org.steria.calculator.service.domain.tax.trinnskat.TrinnskattLevel;

import static org.steria.calculator.service.predicate.TrinnskattPredicates.*;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class TrinnskattCalculatorTest {

    private TaxCalculator<Trinnskatt> calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new TrinnskattCalculatorV1();
    }

    @Test
    public void getL0TrinnskatBoundaryTaxTest() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(100000D));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == 0);
        trinnskatt = calculator.calculate(new Income(TRINNSKAT_L0_TOP));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == 0);
        trinnskatt = calculator.calculate(new Income(TRINNSKAT_L0_TOP-1));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == 0);
    }

    @Test
    public void getL1TrinnskatBoundaryTaxTest() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(TRINNSKAT_L0_TOP +1000));
        Double v = 1000*TrinnskattLevel.TRINN_1.getPst();
        Assert.assertTrue(14 == v);
        Assert.assertTrue(trinnskatt.getEstimatedTax() == 14);

        trinnskatt = calculator.calculate(new Income(TRINNSKAT_L0_TOP +1));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == 0.014);

        trinnskatt = calculator.calculate(new Income(TRINNSKAT_L1_TOP));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == (TRINNSKAT_L1_TOP-TRINNSKAT_L0_TOP)*0.014);
    }

    @Test
    public void getL2TrinnskatBoundaryTaxTest() throws Exception {
        double level1sumTax = (TRINNSKAT_L1_TOP - TRINNSKAT_L0_TOP) * 0.014;
        Trinnskatt trinnskatt = calculator.calculate(new Income(TRINNSKAT_L1_TOP +1000));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == level1sumTax+1000*0.033);

        trinnskatt = calculator.calculate(new Income(TRINNSKAT_L2_TOP));
        double level2sumTax = (TRINNSKAT_L2_TOP - TRINNSKAT_L1_TOP) * 0.033;
        Assert.assertTrue(trinnskatt.getEstimatedTax() == level1sumTax+level2sumTax);
    }

    @Test
    public void getL3TrinnskatBoundaryTaxTest() throws Exception {
        double level1sumTax = (TRINNSKAT_L1_TOP - TRINNSKAT_L0_TOP) * 0.014;
        double level2sumTax = (TRINNSKAT_L2_TOP - TRINNSKAT_L1_TOP) * 0.033;
        double level3sumTax = (TRINNSKAT_L3_TOP - TRINNSKAT_L2_TOP) * 0.124;

        Trinnskatt trinnskatt = calculator.calculate(new Income(TRINNSKAT_L2_TOP +1000));
        Assert.assertTrue(trinnskatt.getEstimatedTax() == level1sumTax+level2sumTax+1000*0.124);

        trinnskatt = calculator.calculate(new Income(TRINNSKAT_L3_TOP));
        Assert.assertEquals(trinnskatt.getEstimatedTax(), level1sumTax+level2sumTax+level3sumTax, 0.1);
    }

    @Test
    public void getL4TrinnskatBoundaryTaxTest() throws Exception {
        double level1sumTax = (TRINNSKAT_L1_TOP - TRINNSKAT_L0_TOP) * 0.014;
        double level2sumTax = (TRINNSKAT_L2_TOP - TRINNSKAT_L1_TOP) * 0.033;
        double level3sumTax = (TRINNSKAT_L3_TOP - TRINNSKAT_L2_TOP) * 0.124;

        Trinnskatt trinnskatt = calculator.calculate(new Income(TRINNSKAT_L3_TOP +2000));
        Assert.assertEquals(trinnskatt.getEstimatedTax(), level1sumTax+level2sumTax+level3sumTax+2000*0.154, 0.1);

        trinnskatt = calculator.calculate(new Income(1000000d));
        Assert.assertEquals(trinnskatt.getEstimatedTax(), 63830, 1);

    }

    @Test
    public void getTrinnskatBasisTaxTest() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(250000d));
        Assert.assertTrue(trinnskatt.getBasis() == 250000);
        trinnskatt = calculator.calculate(new Income(110000d));
        Assert.assertTrue(trinnskatt.getBasis() == 110000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinusInput(){
        calculator.calculate(new Income(-1d));
    }

    @Test
    public void getOneHoundredTrinnskatType() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(100000d));
        Assert.assertEquals(trinnskatt.getCategory(), TrinnskattLevel.TRINN_0);
        Assert.assertTrue(TrinnskattLevel.TRINN_0.getPst() == 0);
    }

    @Test
    public void getTwoHundredTrinnskatType() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(200000d));
        Assert.assertEquals(trinnskatt.getCategory(), TrinnskattLevel.TRINN_1);
        Assert.assertTrue(TrinnskattLevel.TRINN_1.getPst() == 0.014);
    }

    @Test
    public void getFourHundredTrinnskatType() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(400000d));
        Assert.assertEquals(trinnskatt.getCategory(), TrinnskattLevel.TRINN_2);
        Assert.assertTrue(TrinnskattLevel.TRINN_2.getPst() == 0.033);
    }

    @Test
    public void getEightHundredTrinnskatType() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(800000d));
        Assert.assertEquals(trinnskatt.getCategory(), TrinnskattLevel.TRINN_3);
        Assert.assertTrue(TrinnskattLevel.TRINN_3.getPst() == 0.124);
    }

    @Test
    public void getMillionTrinnskatType() throws Exception {
        Trinnskatt trinnskatt = calculator.calculate(new Income(1000000d));
        Assert.assertEquals(trinnskatt.getCategory(), TrinnskattLevel.TRINN_4);
        Assert.assertTrue(TrinnskattLevel.TRINN_4.getPst() == 0.154);
    }

}
