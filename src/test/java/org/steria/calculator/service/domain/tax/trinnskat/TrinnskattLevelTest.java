package org.steria.calculator.service.domain.tax.trinnskat;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.steria.calculator.service.domain.tax.trinnskat.TrinnskattLevel.*;
/**
 * Created by Stanislav Mantach on 07.03.2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class TrinnskattLevelTest {

    @Test
    public void testLevelChunkDevision() throws Exception {

        Assert.assertTrue(getTaxableChunk(170000d, TRINN_0)==TRINN_0.getChunkAmount());
        Assert.assertTrue(getTaxableChunk(1000d, TRINN_1)==1000);

    }
}
