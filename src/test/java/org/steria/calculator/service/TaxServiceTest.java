package org.steria.calculator.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.steria.calculator.service.calculator.TaxCalculator;
import org.steria.calculator.service.calculator.TrinnskattCalculatorV1;
import org.steria.calculator.service.calculator.TrygdeavgiftCalculatorV1;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.TaxCalculation;
import org.steria.calculator.service.domain.tax.trinnskat.Trinnskatt;
import org.steria.calculator.service.domain.tax.trinnskat.TrinnskattLevel;
import org.steria.calculator.service.domain.tax.trygdeavgift.Trygdeavgift;
import org.steria.calculator.service.domain.tax.trygdeavgift.TrygdeavgiftLevel;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class TaxServiceTest {

    //TO DO make unit test with @InjectMocks instead of integration
    private TaxService taxService = new TaxServiceV1();

    //TO DO make unit test with @Mock instead of integration
    private TaxCalculator<Trinnskatt> calculatorTrinnskatt = new TrinnskattCalculatorV1();
    private TaxCalculator<Trygdeavgift> calculatorTrygdeavgift = new TrygdeavgiftCalculatorV1();

    private Income income;

    @Before
    public void setUp() throws Exception {
        TaxServiceV1 taxServiceV1 = new TaxServiceV1();
        taxServiceV1.setTaxCalculatorTrinnskatt(calculatorTrinnskatt);
        taxServiceV1.setTaxCalculatorTrygdeavgift(calculatorTrygdeavgift);
        taxService = taxServiceV1;
        income = new Income(1000000D);
    }

    @Test
    public void setIncomeTest() throws Exception {
        TaxCalculation taxCalculation = taxService.calculate(income);

        Assert.assertNotNull(taxCalculation.getNetIncome());
        Assert.assertNotNull(taxCalculation.getNetIncome().getAmount());
        Assert.assertTrue(taxCalculation.getNetIncome().getAmount()==1000000);
    }

    @Test
    public void addTrinnskatt() throws Exception {
        TaxCalculation taxCalculation = taxService.calculate(income);
        Assert.assertNotNull(taxCalculation.getTaxAndDuties());
        Assert.assertNotNull(taxCalculation.getTaxAndDuties().getTrinnskatt());
    }

    @Test
    public void calculateMillionTrinnskatt() {
        TaxCalculation taxCalculation = taxService.calculate(income);
        Trinnskatt trinnskatt = taxCalculation.getTaxAndDuties().getTrinnskatt();
        Assert.assertTrue(trinnskatt.getCategory().equals(TrinnskattLevel.TRINN_4));
        Assert.assertTrue(trinnskatt.getBasis()==1000000);
        Assert.assertEquals(trinnskatt.getEstimatedTax(), 63830, 1);
    }

    @Test
    public void addTrygdeavgift() throws Exception {
        TaxCalculation taxCalculation = taxService.calculate(income);
        Trygdeavgift trygdeavgift = taxCalculation.getTaxAndDuties().getTrygdeavgift();
        Assert.assertNotNull(trygdeavgift);
    }

    @Test
    public void addTrygdeavgiftBasis() throws Exception {
        TaxCalculation taxCalculation = taxService.calculate(income);
        Trygdeavgift trygdeavgift = taxCalculation.getTaxAndDuties().getTrygdeavgift();
        Assert.assertNotNull(trygdeavgift.getBasis());
    }

    @Test
    public void calculateMillionTrygdeavgift() throws Exception {
        TaxCalculation taxCalculation = taxService.calculate(income);
        Trygdeavgift trygdeavgift = taxCalculation.getTaxAndDuties().getTrygdeavgift();
        Assert.assertTrue(trygdeavgift.getCategory().equals(TrygdeavgiftLevel.TRYG_1));
        Assert.assertTrue(trygdeavgift.getBasis()==1000000);
        Assert.assertEquals(trygdeavgift.getEstimatedTax(), 82000, 1);
    }


    @Test
    public void getTotalTaxToBePaid() throws Exception {
        TaxCalculation taxCalculation = taxService.calculate(income);
        Double expectedTaxForTry = 82000d;
        Double expectedTaxForTrinn = 63830d;
        Assert.assertEquals(taxCalculation.getTotal(), expectedTaxForTry+expectedTaxForTrinn, 1);
    }

}
