package org.steria.calculator.config;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class ApiConstants {

    public interface V_1 {
        String API_V1 = "/api/v1";
        String TAX = API_V1 + "/tax";
    }

}