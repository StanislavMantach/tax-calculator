package org.steria.calculator.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.steria.calculator.config.ApiConstants;
import org.steria.calculator.service.TaxService;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.TaxCalculation;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
@RestController
@RequestMapping(ApiConstants.V_1.TAX)
public class TaxController {

    @Autowired
    private TaxService taxService;

    @GetMapping("/{wageAmount}")
    public ResponseEntity<TaxCalculation> tax(@PathVariable Double wageAmount) {
        try {
            TaxCalculation taxCalculation = taxService.calculate(new Income(wageAmount));
            return new ResponseEntity<>(taxCalculation, HttpStatus.OK);
        } catch (IllegalArgumentException e){
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }


}
