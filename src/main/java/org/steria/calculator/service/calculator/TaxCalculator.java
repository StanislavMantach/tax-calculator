package org.steria.calculator.service.calculator;

import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.tax.trinnskat.Trinnskatt;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public interface TaxCalculator<T> {
    T calculate(Income income);
}
