package org.steria.calculator.service.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.tax.trygdeavgift.Trygdeavgift;
import org.steria.calculator.service.domain.tax.trygdeavgift.TrygdeavgiftLevel;

import java.util.Map;

import static org.steria.calculator.service.predicate.TrygdeavgiftPredicates.TRYGDEAVGIFT_L0_TOP;

/**
 * Created by Stanislav Mantach on 07.03.2018.
 */
@Component
public class TrygdeavgiftCalculatorV1 implements TaxCalculator<Trygdeavgift> {
    @Override
    public Trygdeavgift calculate(Income income) {

        Trygdeavgift trygdeavgift = new Trygdeavgift();
        Double amount = income.getAmount();

        trygdeavgift.setBasis(amount);
        trygdeavgift.setCategory(TrygdeavgiftLevel.from(amount));

        Map<TrygdeavgiftLevel, Double> taxableChunks = TrygdeavgiftLevel.getTaxableChunks(amount);

        Double trygdeavgiftAmount = 0d;

        Double x = taxableChunks.get(TrygdeavgiftLevel.TRYG_1);

        boolean below25pst = x * 0.25 <= (TRYGDEAVGIFT_L0_TOP + x) * TrygdeavgiftLevel.TRYG_1.getPst();

        if (below25pst){
            trygdeavgiftAmount = x*0.25;
        } else {
            trygdeavgiftAmount = (TRYGDEAVGIFT_L0_TOP + x)*TrygdeavgiftLevel.TRYG_1.getPst();
        }

        trygdeavgift.setEstimatedTax(trygdeavgiftAmount);

        return trygdeavgift;
    }
}
