package org.steria.calculator.service.calculator;

import org.springframework.stereotype.Component;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.tax.trinnskat.Trinnskatt;
import org.steria.calculator.service.domain.tax.trinnskat.TrinnskattLevel;

import java.util.Map;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
@Component
public class TrinnskattCalculatorV1 implements TaxCalculator<Trinnskatt> {

    @Override
    public Trinnskatt calculate(Income income) {

        Trinnskatt trinnskatt = new Trinnskatt();
        Double amount = income.getAmount();
        trinnskatt.setBasis(amount);
        trinnskatt.setCategory(TrinnskattLevel.from(amount));

        Map<TrinnskattLevel, Double> taxableChunks = TrinnskattLevel.getTaxableChunks(amount);
        double trinnskattAmount = taxableChunks.entrySet().stream().mapToDouble(entry -> {
            double taxPerTaxableLevel = entry.getValue() * entry.getKey().getPst();
            return taxPerTaxableLevel;
        }).sum();

        trinnskatt.setEstimatedTax(trinnskattAmount);
        return trinnskatt;
    }

    private static double getTaxableChunk(Double amount, TrinnskattLevel level) {
        return amount - level.getChunkAmount()>0?amount - level.getChunkAmount():0;
    }

}
