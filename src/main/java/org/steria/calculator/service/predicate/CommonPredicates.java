package org.steria.calculator.service.predicate;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class CommonPredicates {

    public static boolean amountLessThanZero(Double amount){
        return amount < 0;
    }

}
