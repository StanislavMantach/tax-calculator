package org.steria.calculator.service.predicate;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class TrinnskattPredicates {

    public static final Double TRINNSKAT_L0_TOP = 169000d;
    public static final Double TRINNSKAT_L1_TOP = 237900d;
    public static final Double TRINNSKAT_L2_TOP = 598050d;
    public static final Double TRINNSKAT_L3_TOP = 962050d;
    public static final Double TRINNSKAT_L4_TOP = Double.MAX_VALUE;

    public static boolean trinnLevel0(Double amount){
        return amount < TRINNSKAT_L0_TOP;
    }

    public static boolean trinnLevel1(Double amount){
        return amount >=TRINNSKAT_L0_TOP && amount < TRINNSKAT_L1_TOP;
    }

    public static boolean trinnLevel2(Double amount){
        return amount >=TRINNSKAT_L1_TOP && amount < TRINNSKAT_L2_TOP;
    }

    public static boolean trinnLevel3(Double amount){
        return amount >=TRINNSKAT_L2_TOP && amount < TRINNSKAT_L3_TOP;
    }

    public static boolean trinnLevel4(Double amount){
        return amount >=TRINNSKAT_L3_TOP;
    }

}
