package org.steria.calculator.service.predicate;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class TrygdeavgiftPredicates {

    public static final Double TRYGDEAVGIFT_L0_TOP = 54650d;
    public static final Double TRYGDEAVGIFT_L1_TOP = Double.MAX_VALUE;


    public static boolean trygdLevel0(Double amount){
        return amount < TRYGDEAVGIFT_L0_TOP;
    }

    public static boolean trygdLevel1(Double amount){
        return amount >=TRYGDEAVGIFT_L0_TOP;
    }

}
