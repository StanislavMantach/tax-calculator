package org.steria.calculator.service.domain;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class TaxCalculation {

    private NetIncome netIncome;
    private TaxAndDuties taxAndDuties;
    private Double total;

    public TaxCalculation(Income income) {
        this.netIncome = new NetIncome(income);
    }

    public NetIncome getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(NetIncome netIncome) {
        this.netIncome = netIncome;
    }

    public TaxAndDuties getTaxAndDuties() {
        return taxAndDuties;
    }

    public void setTaxAndDuties(TaxAndDuties taxAndDuties) {
        this.taxAndDuties = taxAndDuties;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
