package org.steria.calculator.service.domain.tax.trinnskat;

import org.steria.calculator.service.domain.tax.BasicTax;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class Trinnskatt extends BasicTax{

    private TrinnskattLevel category;

    public TrinnskattLevel getCategory() {
        return category;
    }

    public void setCategory(TrinnskattLevel category) {
        this.category = category;
    }

}
