package org.steria.calculator.service.domain.tax.trinnskat;

import org.steria.calculator.service.predicate.TrinnskattPredicates;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import static org.steria.calculator.service.predicate.TrinnskattPredicates.*;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public enum TrinnskattLevel{

    TRINN_0(0d, TRINNSKAT_L0_TOP, TrinnskattPredicates::trinnLevel0),
    TRINN_1(0.014, TRINNSKAT_L1_TOP-TRINNSKAT_L0_TOP, TrinnskattPredicates::trinnLevel1),
    TRINN_2(0.033, TRINNSKAT_L2_TOP-TRINNSKAT_L1_TOP, TrinnskattPredicates::trinnLevel2),
    TRINN_3(0.124, TRINNSKAT_L3_TOP-TRINNSKAT_L2_TOP, TrinnskattPredicates::trinnLevel3),
    TRINN_4(0.154, TRINNSKAT_L4_TOP-TRINNSKAT_L3_TOP, TrinnskattPredicates::trinnLevel4);

    TrinnskattLevel(Double pst, Double chunkAmount, Predicate<Double> predicate) {
        this.pst = pst;
        this.chunkAmount = chunkAmount;
        this.predicate = predicate;
    }

    private double pst;
    private Double chunkAmount;
    private Predicate<Double> predicate;

    public Double getChunkAmount() {
        return chunkAmount;
    }

    public Predicate<Double> getPredicate() {
        return predicate;
    }

    public double getPst() {
        return pst;
    }

    public static Map<TrinnskattLevel, Double> getTaxableChunks(Double amount){

        Map<TrinnskattLevel, Double> taxLeveldevision = new HashMap<>();

        taxLeveldevision.put(TRINN_0, getTaxableChunk(amount, TRINN_0));

        amount=amount-getTaxableChunk(amount, TRINN_0);
        taxLeveldevision.put(TRINN_1, getTaxableChunk(amount, TRINN_1));

        amount=amount-getTaxableChunk(amount, TRINN_1);
        taxLeveldevision.put(TRINN_2, getTaxableChunk(amount, TRINN_2));

        amount=amount-getTaxableChunk(amount, TRINN_2);
        taxLeveldevision.put(TRINN_3, getTaxableChunk(amount, TRINN_3));

        amount=amount-getTaxableChunk(amount, TRINN_3);
        taxLeveldevision.put(TRINN_4, amount);

        return taxLeveldevision;
    }

    public static double getTaxableChunk(Double amount, TrinnskattLevel level) {

        Double chunkAmount = level.getChunkAmount();
        if (amount >= chunkAmount){
            return chunkAmount;
        } else {
            return amount-chunkAmount+chunkAmount;
        }

    }

    public static TrinnskattLevel from(Double amount){
      return Arrays.stream(TrinnskattLevel.values())
              .filter(trinnskattLevel -> {
                  return trinnskattLevel.getPredicate().test(amount);
              })
              .findFirst().orElseThrow(() -> new IllegalArgumentException("Can't determine tax level fro: "+ amount));
    }
}
