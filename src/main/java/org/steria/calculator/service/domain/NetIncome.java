package org.steria.calculator.service.domain;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class NetIncome {

    Double amount;

    public NetIncome(Income income) {
        this.amount = income.getAmount();
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
