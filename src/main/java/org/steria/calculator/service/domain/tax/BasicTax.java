package org.steria.calculator.service.domain.tax;

/**
 * Created by Stanislav Mantach on 07.03.2018.
 */
public class BasicTax {

    private Double basis;
    private Double estimatedTax;


    public Double getBasis() {
        return basis;
    }

    public void setBasis(Double basis) {
        this.basis = basis;
    }

    public Double getEstimatedTax() {
        return estimatedTax;
    }

    public void setEstimatedTax(Double estimatedTax) {
        this.estimatedTax = estimatedTax;
    }

}
