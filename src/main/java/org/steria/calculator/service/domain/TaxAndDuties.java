package org.steria.calculator.service.domain;

import org.steria.calculator.service.domain.tax.trinnskat.Trinnskatt;
import org.steria.calculator.service.domain.tax.trygdeavgift.Trygdeavgift;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class TaxAndDuties {
    private Trinnskatt trinnskatt;
    private Trygdeavgift trygdeavgift;

    public Trinnskatt getTrinnskatt() {
        return trinnskatt;
    }

    public void setTrinnskatt(Trinnskatt trinnskatt) {
        this.trinnskatt = trinnskatt;
    }

    public Trygdeavgift getTrygdeavgift() {
        return trygdeavgift;
    }

    public void setTrygdeavgift(Trygdeavgift trygdeavgift) {
        this.trygdeavgift = trygdeavgift;
    }
}
