package org.steria.calculator.service.domain.tax.trygdeavgift;

import org.steria.calculator.service.domain.tax.BasicTax;

/**
 * Created by Stanislav Mantach on 07.03.2018.
 */
public class Trygdeavgift extends BasicTax {

    private TrygdeavgiftLevel category;

    public TrygdeavgiftLevel getCategory() {
        return category;
    }

    public void setCategory(TrygdeavgiftLevel category) {
        this.category = category;
    }

}
