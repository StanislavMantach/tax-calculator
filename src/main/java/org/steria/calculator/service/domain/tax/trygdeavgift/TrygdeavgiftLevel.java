package org.steria.calculator.service.domain.tax.trygdeavgift;

import org.steria.calculator.service.domain.tax.trinnskat.TrinnskattLevel;
import org.steria.calculator.service.predicate.TrygdeavgiftPredicates;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import static org.steria.calculator.service.predicate.TrygdeavgiftPredicates.TRYGDEAVGIFT_L0_TOP;
import static org.steria.calculator.service.predicate.TrygdeavgiftPredicates.TRYGDEAVGIFT_L1_TOP;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public enum TrygdeavgiftLevel {

    TRYG_0(0d, TRYGDEAVGIFT_L0_TOP, TrygdeavgiftPredicates::trygdLevel0),
    TRYG_1(0.082, TRYGDEAVGIFT_L1_TOP, TrygdeavgiftPredicates::trygdLevel1), ;

    TrygdeavgiftLevel(Double pst, Double chunkAmount, Predicate<Double> predicate) {
        this.pst = pst;
        this.chunkAmount = chunkAmount;
        this.predicate = predicate;
    }

    private double pst;
    private Double chunkAmount;
    private Predicate<Double> predicate;

    public Double getChunkAmount() {
        return chunkAmount;
    }

    public Predicate<Double> getPredicate() {
        return predicate;
    }

    public double getPst() {
        return pst;
    }

    public static Map<TrygdeavgiftLevel, Double> getTaxableChunks(Double amount){
        Map<TrygdeavgiftLevel, Double> taxLeveldevision = new HashMap<>();

        taxLeveldevision.put(TRYG_0, getTaxableChunk(amount, TRYG_0));

        amount=amount-getTaxableChunk(amount, TRYG_0);
        taxLeveldevision.put(TRYG_1, amount);

        return taxLeveldevision;
    }

    public static double getTaxableChunk(Double amount, TrygdeavgiftLevel level) {
        Double chunkAmount = level.getChunkAmount();
        if (amount >= chunkAmount){
            return chunkAmount;
        } else {
            return amount-chunkAmount+chunkAmount;
        }
    }

    public static TrygdeavgiftLevel from(Double amount){
        return Arrays.stream(TrygdeavgiftLevel.values())
                .filter(trinnskattLevel -> trinnskattLevel.getPredicate().test(amount))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Can't determine tax level fro: "+ amount));
    }

}
