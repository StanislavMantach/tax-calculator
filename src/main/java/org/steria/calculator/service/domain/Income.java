package org.steria.calculator.service.domain;

import org.steria.calculator.service.predicate.CommonPredicates;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public class Income {
    private Double amount;

    public Income(Double amount) {
        if (CommonPredicates.amountLessThanZero(amount)) throw new IllegalArgumentException("Income cannot be less than zero");
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
