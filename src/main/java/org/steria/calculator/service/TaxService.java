package org.steria.calculator.service;

import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.TaxCalculation;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
public interface TaxService {
    TaxCalculation calculate(Income income);
}
