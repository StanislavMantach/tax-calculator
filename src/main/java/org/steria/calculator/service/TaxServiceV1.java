package org.steria.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.steria.calculator.service.calculator.TaxCalculator;
import org.steria.calculator.service.domain.Income;
import org.steria.calculator.service.domain.TaxAndDuties;
import org.steria.calculator.service.domain.TaxCalculation;
import org.steria.calculator.service.domain.tax.trinnskat.Trinnskatt;
import org.steria.calculator.service.domain.tax.trinnskat.TrinnskattLevel;
import org.steria.calculator.service.domain.tax.trygdeavgift.Trygdeavgift;

/**
 * Created by Stanislav Mantach on 06.03.2018.
 */
@Service
public class TaxServiceV1 implements TaxService {

    @Autowired
    private TaxCalculator<Trinnskatt> taxCalculatorTrinnskatt;


    @Autowired
    private TaxCalculator<Trygdeavgift> taxCalculatorTrygdeavgift;

    @Override
    public TaxCalculation calculate(Income income) {

        TaxCalculation taxCalculation = new TaxCalculation(income);
        TaxAndDuties taxAndDuties = calculateSkattOgAvgift(income);
        taxCalculation.setTaxAndDuties(taxAndDuties);
        taxCalculation.setTotal(taxAndDuties.getTrinnskatt().getEstimatedTax()+taxAndDuties.getTrygdeavgift().getEstimatedTax());

        return taxCalculation;
    }

    private TaxAndDuties calculateSkattOgAvgift(Income income) {
        TaxAndDuties taxAndDuties = new TaxAndDuties();

        setTrinnskatt(income, taxAndDuties);
        setTrygdeavgift(income, taxAndDuties);

        return taxAndDuties;
    }

    private void setTrygdeavgift(Income income, TaxAndDuties taxAndDuties) {
        taxAndDuties.setTrygdeavgift(taxCalculatorTrygdeavgift.calculate(income));
    }

    private void setTrinnskatt(Income income, TaxAndDuties taxAndDuties) {
        Trinnskatt trinnskatt = taxCalculatorTrinnskatt.calculate(income);
        trinnskatt.setCategory(TrinnskattLevel.from(income.getAmount()));
        taxAndDuties.setTrinnskatt(trinnskatt);
    }

    void setTaxCalculatorTrinnskatt(TaxCalculator<Trinnskatt> taxCalculatorTrinnskatt) {
        this.taxCalculatorTrinnskatt = taxCalculatorTrinnskatt;
    }

    public void setTaxCalculatorTrygdeavgift(TaxCalculator<Trygdeavgift> taxCalculatorTrygdeavgift) {
        this.taxCalculatorTrygdeavgift = taxCalculatorTrygdeavgift;
    }

}
